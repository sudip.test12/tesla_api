const controller = require('../controllers/users/users')
const validate = require('../controllers/users/users.validate')
const AuthController = require('../controllers/auth/auth')
const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

/*
 * Users routes
 */

/* Get items route */
router.get('/', requireAuth, AuthController.roleAuthorization([1]), trimRequest.all, controller.getItems)

/* Create new item route */
router.post('/', requireAuth, AuthController.roleAuthorization([1]), trimRequest.all, validate.createItem, controller.createItem)

/* Get item route */
router.get('/:id', requireAuth, AuthController.roleAuthorization([0, 1]), trimRequest.all, validate.getItem, controller.getItem)

/* User Veriification */
router.get('/verification/:id', requireAuth, trimRequest.all, validate.getItem, controller.getItem)

/* Update item route */
router.patch('/:id', requireAuth, AuthController.roleAuthorization([0, 1]), trimRequest.all, validate.updateItem, controller.updateItem)

/* Delete item route */
router.delete('/:id', requireAuth, AuthController.roleAuthorization([1]), trimRequest.all, validate.deleteItem, controller.deleteItem)

module.exports = router
