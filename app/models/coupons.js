const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { ModifiedBySchema } = require('./common')

const FilterSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    message: {
      type: String,
      required: false
    },
    tnc: {
      type: String,
      required: false
    },
    valid_from: {
      type: Date,
      required: true
    },
    valid_till: {
      type: Date,
      required: true
    },
    entered_by: {
      type: ModifiedBySchema,
      default: null
    },
    modified_by: {
      type: [ModifiedBySchema],
      default: null
    },
    is_active: {
      type: Number,
      default: 1
    },
    when_entered: {
      type: Date,
      default: Date.now
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)
FilterSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Coupons', FilterSchema)
