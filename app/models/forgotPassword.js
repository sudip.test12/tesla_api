const mongoose = require('mongoose')
const validator = require('validator')

const ForgotPasswordSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      validate: {
        validator: validator.isEmail,
        message: 'EMAIL_IS_NOT_VALID'
      },
      lowercase: true,
      required: true
    },
    verification: {
      type: String
    },
    used: {
      type: Boolean,
      default: false
    },
    ip_request: {
      type: String
    },
    browser_request: {
      type: String
    },
    country_request: {
      type: String
    },
    ip_changed: {
      type: String
    },
    browser_changed: {
      type: String
    },
    country_changed: {
      type: String
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
module.exports = mongoose.model('ForgotPassword', ForgotPasswordSchema)
