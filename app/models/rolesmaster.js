const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { ModifiedBySchema } = require('./common')

const RolesMasterSchema = new mongoose.Schema(
  {
    role_name: {
      type: String,
      required: true
    },
    is_super_admin: {
      type: Number,
      required: true
    },
    module_access_ids: {
      type: String,
      required: false
    },
    entered_by: {
      type: ModifiedBySchema,
      default: null
    },
    when_modified: {
      type: Date,
      default: null
    },
    modified_by: {
      type: [ModifiedBySchema],
      default: null
    },
    is_active: {
      type: Number,
      default: 1
    },
    when_entered: {
      type: Date,
      default: null
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)
RolesMasterSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('RolesMaster', RolesMasterSchema)
