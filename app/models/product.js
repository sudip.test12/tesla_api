const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { ModifiedBySchema } = require('./common')

const AttachmentSchema = {
    low_res: {
        type: String,
        required: false
    },
    high_res: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: false
    },
    type: {
        enum: ['image', 'pdf', 'mp4', 'mp3'],
        required: false
    },
    sequence: {
        type: Number,
        required: true
    },
}

const DimensionsSchema = {
    size: {
        type: String,
        required: false,
    },
    size_unit: {
        enum: ['cm', 'm', 'mm'],
        required: false,
    },
    size_type: {
        enum: ['length', 'height', 'width', ],
        required: false,
    },
}

/**
 * material: Created materials for clothing material, ingredients of food, chemicals, etc, 
 */
const ProductSpecificationSchema = {
    material: {
        type: String,
        required: false,
    },
    pattern: {
        type: String,
        required: false,
    },
    dimensions: {
        type: String,
        required: false,
    }
}

const ProductSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true
        },
        brand: {
            type: String,
            required: true
        },
        custom_label: {
            type: String,
            required: false
        },
        description: {
            type: String,
            required: false
        },
        keywords: {
            type: Array,
            required: false
        },
        product_type: {
            type: String,
            required: true
        },
        gender: {
            type: String,
            enum: ['unisex', 'male', 'female'],
            default: 'unisex',
            required: true
        },
        attachments: {
            type: [],
            required: false
        },
        canonical_short_link: {
            type: String,
            required: false,
        },
        specifications: {
            type: ProductSpecificationSchema,
            required: false
        },
        dimensions: {
            type: [],
            required: false,
        },
        entered_by: {
            type: ModifiedBySchema,
            default: null
        },
        when_modified: {
            type: Date,
            default: null
        },
        modified_by: {
            type: [ModifiedBySchema],
            default: null
        },
        is_active: {
            type: Number,
            default: 1
        },
        when_entered: {
            type: Date,
            default: null
        },
    },
    {
        versionKey: false,
        timestamps: true
    }
)
ProductSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Product', ProductSchema)
