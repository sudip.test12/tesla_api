const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const NotificationSchema = new mongoose.Schema(
  {
    title: {
      type: String
    },
    message : {
      type: String,
      required:true
    },
    description : {
      type: String
    },
    user_ids : {
      type: Array,
    },
    sent_type : {
      type: Number,
    },
    noti_type : {
      type: Number,
      required:true
    },
    modified_by : {
      type: Array,
      required:true
    },
    when_entered : {
      type: Date,
      default: Date.now
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
NotificationSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Notifications', NotificationSchema)
