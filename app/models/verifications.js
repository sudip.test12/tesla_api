const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.ObjectId;
const mongoosePaginate = require('mongoose-paginate-v2')

const DetailsSchema = new mongoose.Schema({
  full_name: {
    type: String,
    required:true
  },
  card_no: {
    type: Number
  },
  img: {
    type: String
  },
})

const VerificationSchema = new mongoose.Schema(
  {
    type: {
      type: Number
    },
    details : {
      type: DetailsSchema,
      required:true
    },
    verified : {
      type: Number,
      default:0
    },
    user_id : {
      type: ObjectId,
      required:true
    },
    sent_type : {
      type: Number,
    },
    noti_type : {
      type: Number,
      required:true
    },
    modified_by : {
      type: Array,
      required:true
    },
    when_entered : {
      type: Date,
      default: Date.now
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
VerificationSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Verification', VerificationSchema)
