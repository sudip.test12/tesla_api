const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.ObjectId;
const bcrypt = require('bcrypt');
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate-v2');
const auth = require('../middleware/auth')

const ModifiedBySchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true
    },
    admin_id: {
      type: ObjectId,
      required: true
    },
    modified_date: {
      type: Date,
      default: Date.now
    },
  }
);

const CardsSchema = new Schema({
  card_id: {
    type: String,
    required: true
  },
  card_no: {
    type: String,
    required: true
  },
  name_on_card: {
    type: String,
    required: true
  },
  valid: {
    type: String,
    required: true
  },
})

const PANCardDetailsSchema = new Schema({
  full_name: {
    type: String,
    required: true
  },
  card_no: {
    type: String,
    required: true
  },
  img: {
    type: String,
    required: true
  },
});

const ReviewNRatingsSchema = new Schema({
  rid: {
    type: Number,
    required: true
  },
  title: {
    type: String
  },
  description: {
    type: String
  },
  rating: {
    type: Number
  },
  productId: {
    type: String,
    required: true
  },
  uploads: {
    type: Array,
  }
})

const VerificationSchema = new Schema({
  v_email: {
    type: Number,
    default: 0
  },
  v_mobile: {
    type: Number,
    default: 0
  }
})

const AddressDetailsSchema = new Schema({
  a_id: {
    type: Number
  },
  default: {
    type: Number
  },
  line_1: {
    type: String
  },
  line_2: {
    type: String
  },
  city: {
    type: String
  },
  state: {
    type: String
  },
  pincode: {
    type: String
  },
  landmark: {
    type: String
  },
  type: {
    type: String
  },
});

const UserSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  mobile: {
    type: String,
    default: ""
  },
  alt_mobile: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    validate: {
      validator: validator.isEmail,
      message: 'EMAIL_IS_NOT_VALID'
    },
    lowercase: true,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  f_password: {
    type: String,
    required: true,
    select: false
  },
  gender: {
    type: Number,
    default: 0
  },
  address_details: {
    type: [AddressDetailsSchema],
  },
  pan_card_details: {
    type: PANCardDetailsSchema,
    default: null,
    required: false,
  },
  cards: {
    type: [CardsSchema],
    default: null
  },
  verification: {
    type: VerificationSchema
  },
  wishlist: {
    type: [ObjectId],
    default: []
  },
  login_attempts: {
    type: Number,
    default: 0,
  },
  role: {
    type: Number,
    //0-User, 1-Admin 
    default: 0
  },
  block_expires: {
    type: Date,
    select: false,
    default: null
  },
  when_entered: {
    type: Date,
    default: Date.now
  },
  when_modified: {
    type: Date,
    select: false,
    default: null
  },
  modified_by: {
    type: [ModifiedBySchema],
    default: []
  },
  deactivated_by: {
    type: [ModifiedBySchema],
    default: []
  },
  is_active: {
    type: Number,
    default: 1
  },
}, 
{
  versionKey: false,
  timestamps: false
})


const hash = (user, salt, next) => {
  bcrypt.hash(user.password, salt, (error, newHash) => {
    if (error) {
      return next(error)
    }
    user.password = newHash
    return next()
  })
}

const genSalt = (user, SALT_FACTOR, next) => {
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err)
    }
    return hash(user, salt, next)
  })
}

UserSchema.pre('save', function (next) {
  const that = this
  const SALT_FACTOR = 5
  if (!that.isModified('password')) {
    return next()
  }
  return genSalt(that, SALT_FACTOR, next)
})

UserSchema.methods.comparePassword = function (passwordAttempt, cb) {
  auth.encrypt1(passwordAttempt, (hash) => {
    var x = this.toObject()
    cb(hash == x.Password)
  })
}

UserSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('User', UserSchema, 'User')
