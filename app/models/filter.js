const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { ModifiedBySchema } = require('./common')

const FilterSchema = new mongoose.Schema(
  {
    filter_name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: false
    },
    is_filter_search: {
      type: Number,
      required: true
    },
    entered_by: {
      type: ModifiedBySchema,
      default: null
    },
    when_modified: {
      type: Date,
      default: null
    },
    modified_by: {
      type: [ModifiedBySchema],
      default: null
    },
    is_active: {
      type: Number,
      default: 1
    },
    when_entered: {
      type: Date,
      default: null
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)
FilterSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Filter', FilterSchema)
