const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;
const bcrypt = require('bcrypt')
const validator = require('validator')
const mongoosePaginate = require('mongoose-paginate-v2')

const ModifiedBySchema = new mongoose.Schema(
  {
    UserName: {
      type: String,
      required: true
    },
    AdminId: {
      type: ObjectId,
      required: true
    },
    modified_date: {
      type: Date,
      default: Date.now
    },
  }
);

const AdminSchema = new mongoose.Schema(
  {
    UserName: {
      type: String,
      required: true
    },
    RoleID: {
      type: ObjectId,
      required: true
    },
    RoleName: {
      type: String
    },
    Password: {
      type: String
    },
    EmailID: {
      type: String,
      validate: {
        validator: validator.isEmail,
        message: 'EMAIL_IS_NOT_VALID'
      },
      lowercase: true,
      unique: true,
      required: true
    },
    Mobile: {
      type: String,
      required: true,
      select: false
    },
    when_entered: {
      type: Date,
      default: Date.now
    },
    modified_by: {
      type: [ModifiedBySchema],
      default: []
    },
    deactivated_by: {
      type: [ModifiedBySchema],
      default: []
    },
    is_active: {
      type: Number,
      default: 1
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)



const genSalt = (user, SALT_FACTOR, next) => {
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err)
    }
    return hash(user, salt, next)
  })
}

AdminSchema.pre('save', function (next) {
  const that = this
  const SALT_FACTOR = 5
  if (!that.isModified('password')) {
    return next()
  }
  return genSalt(that, SALT_FACTOR, next)
})

AdminSchema.methods.comparePassword = function (passwordAttempt, cb) {
  bcrypt.compare(passwordAttempt, this.password, (err, isMatch) =>
    err ? cb(err) : cb(null, isMatch)
  )
}
AdminSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('AdminMaster', AdminSchema)
