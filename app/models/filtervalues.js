const mongoose = require('mongoose')
const ObjectId = require('mongoose').Types.ObjectId
const mongoosePaginate = require('mongoose-paginate-v2')
const { ModifiedBySchema } = require('./common')

const FilterValueSchema = new mongoose.Schema(
  {
    FilterID: {
      type: ObjectId,
      required: true
    },
    FilterValue: {
      type: String,
      required: true
    },
    Keywords: {
      type: String,
      required: true
    },
    Description: {
      type: String,
      default: ""
    },
    IsFilterValueSearch: {
      type: Number,
      default: 1
    },
    ImageName: {
      type: String,
      default: ""
    },
    SortId: {
      type: Number,
      default: 1
    },
    IsHotFilter: {
      type: Number,
      default: 1
    },
    MetaTitle: {
      type: String,
      default: ""
    },
    when_entered: {
      type: Date,
      default: null
    },
    when_modified: {
      type: Date,
      default: null
    },
    entered_by: {
      type: ModifiedBySchema,
      default: null
    },
    modified_by: {
      type: [ModifiedBySchema],
      default: null
    },
    is_active: {
      type: Number,
      default: 1
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
FilterValueSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('FilterValue', FilterValueSchema)
