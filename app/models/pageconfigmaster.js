const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const { ModifiedBySchema } = require('./common')

const PageConfigMasterSchema = new mongoose.Schema(
  {
    module_name: {
      type: String,
      required: true
    },
    module_link: {
      type: String,
      required: false
    },
    main_menu_name: {
      type: String,
      required: false
    },
    submenu_name: {
      type: String,
      required: false
    },
    sort_id: {
      type: Number,
      required: true
    },
    entered_by: {
      type: ModifiedBySchema,
      default: null
    },
    when_modified: {
      type: Date,
      default: null
    },
    modified_by: {
      type: [ModifiedBySchema],
      default: []
    },
    deactivated_by: {
      type: [ModifiedBySchema],
      default: []
    },
    is_active: {
      type: Number,
      default: 1
    },
    when_entered: {
      type: Date,
      default: Date.now
    },
  },
  {
    versionKey: false,
    timestamps: true
  }
)
PageConfigMasterSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('PageConfigMaster', PageConfigMasterSchema)
