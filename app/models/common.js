const { date } = require('faker')
const mongoose = require('mongoose')
const ObjectId = require('mongoose').Types.ObjectId;
const mongoosePaginate = require('mongoose-paginate-v2')

const ModifiedBySchema = {
    user_name: {
      type: String,
      required: true
    },
    admin_id: {
      type: ObjectId,
      required: true
    },
    modified_date: {
      type: Date,
      default: Date.now
    },
  }
  
module.exports = {
  ModifiedBySchema,
}
