const { createChannel } = require("./amqp");

const consume = async () => {
  try {
    const queue = 'mailer';
    const channel = createChannel(queue)
    await channel.assertQueue(queue, { durable: false });
    await channel.consume(
      queue,
      (message) => {
        if (message) {
          console.log(
            " [x] Received '%s'",
            JSON.parse(message.content.toString())
          );
          channel.ack(message);
        }
      },
      { noAck: true }
    );

    console.log(" [*] Waiting for messages. To exit press CTRL+C");
  } catch (err) {
    console.warn(err);
  }
}

consume();