var amqp = require('amqplib/callback_api');

let messageQueueConnection = null;


function getMessageQueueConnection() {
    return new Promise((resolve, reject) => {
        if (!messageQueueConnection) {
            amqp.connect('amqp://localhost', function (error0, connection) {

                if (error0) {
                    throw error0;
                }
                messageQueueConnection = connection;
                resolve(messageQueueConnection)
            });
        } else {
            resolve(messageQueueConnection);
        }
    })
}

function createChannel(queueName) {
    return new Promise((resolve, reject) => {
        getMessageQueueConnection().then((conn) => {
            conn.createChannel(function (error1, channel) {
                if (error1) {
                    throw error1;
                }
                channel.assertQueue(queueName);
                resolve(channel);
            });
        });
    })
}


process.once("SIGINT", async () => {
    await channel.close();
    await connection.close();
});

module.exports = { getMessageQueueConnection, createChannel }


