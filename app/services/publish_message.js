const { createChannel } = require("./amqp")

const sendEmail = () => {
    const queue = 'mailer';
    return new Promise((resolve, reject) => {
        createChannel(queue).then((channel) => {
            channel.sendToQueue(queue, Buffer.from(JSON.stringify({ email: 'sudip.test12@gmail.com' })), (err, result) => {
                resolve(result)
            })
        })
    })
}

module.exports = {sendEmail} 