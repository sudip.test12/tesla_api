# Tesla_Api

# Completed Models
AdminMaster
Coupons
User
Notifications
Order   - Pending
PageConfigMaster
Products = Pending
ReviewNRatings - Pending
RolesMaster - Pending   
Verifications
Filter
FilterValues

# Use Pm2 to start both Server and receive Rabbitmq Message
From Root of the project Run

# To get list of all servers running using pm2
npx pm2 list

# To run new server or Nodejs scripts 
npx pm2 start app/services/receive_message.js

# Get port of running process
sudo netstat -ano -p tcp | grep 17760


RabbitMQ: Run Management server
rabbitmq-server restart


Best Practices
JWT Auth
Rate Limiter
Captcha integration
Node Cluster
Worker Threads
Message Queue
Redis Cache
Design Patterns
    - Adapter Pattern
    - Factory Pattern